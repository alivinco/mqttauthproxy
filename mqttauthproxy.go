package main

import (
	"fmt"
	"net/http"

	log "github.com/Sirupsen/logrus"
	"github.com/alivinco/mqttauthproxy/handlers/emqauth"
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
)

func initRoutes(e *echo.Echo) {
	e.POST("/mqtt/auth", emqauth.AuthHandler)
	e.POST("/mqtt/acl", emqauth.ACLHandler)
	e.POST("/mqtt/superuser", emqauth.SuperuserHandler)
	e.GET("/mqtt/reload_superusers", emqauth.ReloadSuperusersHandler)
}

func main() {
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, ForceColors: true})
	log.SetLevel(log.DebugLevel)
	e := echo.New()
	v := viper.New()
	v.SetConfigName("mqttauthproxy")
	v.AddConfigPath(".")
	err := v.ReadInConfig() // Find and read the config file
	if err != nil {         // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	log.Info(v.AllKeys())
	emqauth.OpendjAPI.SetConfig(v)
	emqauth.OpendjAPI.LoadSuperUsers()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "It works")
	})
	initRoutes(e)
	log.Info("Starting service on port 5005")
	e.Run(standard.New(v.GetString("server.bind_address")))
}
