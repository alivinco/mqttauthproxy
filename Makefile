GOPATH=$(shell pwd)/../../../../
export GOPATH
export PATH=$PATH:$GOPATH/bin

.PHONY : clean

clean :
	rm -Rf $(GOPATH)bin/*
	rm -Rf bin

install-glide :
    curl https://glide.sh/get | sh

init-project :
    glide init

build :
	go install

docker-clean:
	docker rm greenhome
	docker rmi alivinco/greenhome

dist-docker :
	mkdir -p bin
	env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -v -o bin/greenhome
	echo $(shell ls -a bin/)
	docker build -t alivinco/mqttauthproxy .

docker-publish : dist-docker
	docker push alivinco/mqttauthproxy
