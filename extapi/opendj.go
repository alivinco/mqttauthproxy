package extapi

import (
	errorsp "errors"
	"fmt"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/parnurzeal/gorequest"
	"github.com/spf13/viper"
)

// GroupResponse struct is what is returned by https://ldap.qa.service.smartly.no/ldap-mqtt/messagesightroles/
type GroupResponse struct {
	Rev         string `json:"_rev"`
	ID          string `json:"_id"`
	DisplayName string `json:"displayName"`
	Meta        struct {
		LastModified time.Time `json:"lastModified"`
		Created      time.Time `json:"created"`
	} `json:"meta"`
	Members []struct {
		ID string `json:"_id"`
	} `json:"members"`
}

// OpenDjClientResponse is respnse struct from
// https://ldap.qa.service.smartly.no/ldap-mqtt/messagesightclients/N3B10284
//
// {
// _rev: "0000000097d9850c",
// _id: "N3B10284",
// messageSightClientType: "gateway",
// userPassword: "{SSHA}6vVdu0TNd3GwKD5Qri00qjpdrBsFbNylzVR88A==",
// smartlyDeviceType: "door",
// smartlyDeviceModel: "NorDan Smart Door",
// smartlySerialNumber: "N3B10284",
// meta: {
// created: "2016-09-07T06:51:59Z"
// }
// }
type OpenDjClientResponse struct {
	Rev                    string `json:"_rev"`
	ID                     string `json:"_id"`
	MessageSightClientType string `json:"messageSightClientType"`
	UserPassword           string `json:"userPassword"`
	SmartlyDeviceType      string `json:"smartlyDeviceType"`
	SmartlyDeviceModel     string `json:"smartlyDeviceModel"`
	SmartlySerialNumber    string `json:"smartlySerialNumber"`
	Meta                   struct {
		Created time.Time `json:"created"`
	} `json:"meta"`
}

// OpenDjAPI - Open DJ api
type OpenDjAPI struct {
	superUserCache  map[string]bool
	superUserGroups []string
	configs         *viper.Viper
}

// NewOpenDjAPI constructor
func NewOpenDjAPI(superUserGroupsP []string) *OpenDjAPI {
	oapi := OpenDjAPI{superUserGroups: superUserGroupsP}
	return &oapi
}

// LoadSuperUsers loads superusers into cache
func (oapi *OpenDjAPI) LoadSuperUsers() error {
	groupResponse := GroupResponse{}
	oapi.superUserCache = make(map[string]bool)
	var errors []error
	// Get("https://ldap.qa.service.smartly.no/ldap-mqtt/messagesightroles/" + group).

	for _, group := range oapi.superUserGroups {
		resp, _, errs := gorequest.New().Timeout(20*time.Second).
			SetBasicAuth(oapi.configs.GetString("opendjapi.adminuser"), oapi.configs.GetString("opendjapi.adminpassword")).
			Get(oapi.configs.GetString("opendjapi.root_url") + "/ldap-mqtt/messagesightroles/" + group).
			EndStruct(&groupResponse)
		for _, err := range errs {
			if err != nil {
				log.Errorf("Failed to lookup users for group %s : %v", group, err)
			}
		}
		if len(errs) > 0 {
			errors = append(errors, errs...)
		} else {

			if resp.StatusCode == 200 {
				for _, user := range groupResponse.Members {
					oapi.superUserCache[user.ID] = true
				}
			} else {
				log.Errorf("Error response from WS while getting list of superusers . code = %d , response = %s", resp.StatusCode, resp.Status)
				return errorsp.New(fmt.Sprintf("Error code = %d", resp.StatusCode))
			}
		}

	}
	if len(errors) > 0 {
		return errors[0]
	}
	log.Infof(" %d users were loaded into cache with superuser role ", len(oapi.superUserCache))
	return nil
}

// SetConfigs should be used to set configurations
func (oapi *OpenDjAPI) SetConfig(configs *viper.Viper) {
	oapi.configs = configs
	oapi.superUserGroups = configs.GetStringSlice("opendjapi.superuser_groups")
}

// IsSuperuser check username against list of superusers and return true if it has match or false otherwise
func (oapi *OpenDjAPI) IsSuperuser(username string) bool {
	return oapi.superUserCache[username]
}

// GetClient get full client info .
func (oapi *OpenDjAPI) GetClient(username, password string) (*OpenDjClientResponse, int) {
	openDjClient := OpenDjClientResponse{}
	resp, _, errs := gorequest.New().Timeout(20*time.Second).
		SetBasicAuth(username, password).
		Get(oapi.configs.GetString("opendjapi.root_url") + "/ldap-mqtt/messagesightclients/" + username).
		EndStruct(&openDjClient)
	for _, err := range errs {
		if err != nil {
			log.Errorf("Auth failed with error : %s", err)
			return nil, 401
		}
	}
	if resp.StatusCode != 200 {
		log.Errorf("Auth failed with status code = %d", resp.StatusCode)
		return nil, 401
	}
	return &openDjClient, 200
}
