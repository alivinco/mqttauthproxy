package emqauth

import "time"

// OpenDjClientResponse is respnse struct from
// https://ldap.qa.service.smartly.no/ldap-mqtt/messagesightclients/N3B10284
//
// {
// _rev: "0000000097d9850c",
// _id: "N3B10284",
// messageSightClientType: "gateway",
// userPassword: "{SSHA}6vVdu0TNd3GwKD5Qri00qjpdrBsFbNylzVR88A==",
// smartlyDeviceType: "door",
// smartlyDeviceModel: "NorDan Smart Door",
// smartlySerialNumber: "N3B10284",
// meta: {
// created: "2016-09-07T06:51:59Z"
// }
// }
type OpenDjClientResponse struct {
	Rev                    string `json:"_rev"`
	ID                     string `json:"_id"`
	MessageSightClientType string `json:"messageSightClientType"`
	UserPassword           string `json:"userPassword"`
	SmartlyDeviceType      string `json:"smartlyDeviceType"`
	SmartlyDeviceModel     string `json:"smartlyDeviceModel"`
	SmartlySerialNumber    string `json:"smartlySerialNumber"`
	Meta                   struct {
		Created time.Time `json:"created"`
	} `json:"meta"`
}
