package emqauth

import (
	"fmt"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/alivinco/mqttauthproxy/extapi"
	"github.com/labstack/echo"
)

var accessMap = map[string]string{"1": "sub", "2": "pub"}
var OpendjAPI = extapi.NewOpenDjAPI([]string{"nbclients"})

// AuthHandler expects JSON of structure as input
// {auth_req, [
//   {method, post},
//   {url, "http://localhost:8080/mqtt/auth"},
//   {params, [
//     {clientid, "%c"},
//     {username, "%u"},
//     {password, "%P"}
//   ]}
// ]}.
func AuthHandler(c echo.Context) error {
	clientID := c.FormValue("clientid")
	username := c.FormValue("username")
	password := c.FormValue("password")

	// Get user from https://ldap.qa.service.smartly.no/ldap-mqtt/messagesightclients/N3B10284
	// hash password and compare it with response
	//
	log.Infof("Auth request from clientId = %s , username = %s , password = %s", clientID, username, password)
	openDjClient, code := OpendjAPI.GetClient(username, password)
	if code == 200 {
		log.Infof("Successfull login.Device type = %s,id = %s , serial = %s", openDjClient.SmartlyDeviceType, openDjClient.ID, openDjClient.SmartlySerialNumber)
	}
	return c.NoContent(code)
}

// ACLHandler expect JSON of structure as input
// {acl_req, [
//   {method, post},
//   {url, "http://localhost:8080/mqtt/acl"},
//   {params, [
//     {access,   "%A"},
//     {username, "%u"},
//     {clientid, "%c"},
//     {ipaddr,   "%a"},
//     {topic,    "%t"}
//   ]}
// ]}.
func ACLHandler(c echo.Context) error {
	clientID := c.FormValue("clientid")
	// sub = 1 , pub = 2
	access := c.FormValue("access")
	username := c.FormValue("username")
	// ipaddr := c.FormValue("ipaddr")
	topic := c.FormValue("topic")
	topicSplit := strings.SplitN(topic, "/", 2)
	log.Debugf("ACL request from clientId = %s , username = %s , topic = %s ,access = %s", clientID, username, topic, access)
	if len(topicSplit) == 2 {
		if username == topicSplit[0] {
			return c.NoContent(200)
		}
	}
	log.Infof("Username %s is not allowed to perform operation %s on topic %s", username, accessMap[access], topic)
	return c.NoContent(401)
}

// SuperuserHandler expect username and clientID in for of POST form
// {super_req, [
//   {method, post},
//   {url, "http://localhost:8080/mqtt/superuser"},
//   {params, [
//     {username, "%u"},
//     {clientid, "%c"}
//   ]}
// ]}.
func SuperuserHandler(c echo.Context) error {
	clientID := c.FormValue("clientid")
	username := c.FormValue("username")
	// group nbclients - are backend superusers
	// group gwclients - gateways
	// TODO: Check if user belong to nbclients group then return 200 else 401
	log.Debugf("Superuser request from clientId = %s , username = %s", clientID, username)
	if OpendjAPI.IsSuperuser(username) {
		log.Debugf("User %s is superuser", username)
		return c.NoContent(200)
	}
	return c.NoContent(401)
}

func ReloadSuperusersHandler(c echo.Context) error {
	err := OpendjAPI.LoadSuperUsers()
	if err == nil {
		return c.HTML(200, "<b>Reloaded.</b>")
	}
	return c.String(500, fmt.Sprint(err))
}
